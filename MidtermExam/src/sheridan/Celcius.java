package sheridan;

public class Celcius {

	public static void main(String[] args) {
		System.out.println("Temperature in celcius is " + convertFromFahrenheit(7));

	}
	
	public static int convertFromFahrenheit(int n) {
		double c = 5.0 / 9.0 * (n - 32.0);
		return (int) Math.ceil(c);
	}

}
