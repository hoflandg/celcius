package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class CelciusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	// Regular
	@Test
	public void testConvertFromFahrenheit() {
		int c = Celcius.convertFromFahrenheit(32);
		assertTrue("Not a valid fahrenheit temperature", c == 0);
	}
	
	// Exception
	@Test
	public void testConvertFromFahrenheitException() {
		int c  = Celcius.convertFromFahrenheit((int)12.3);
		assertFalse("Valid fahrenheit temperature", c == 11);
	}
	
	// Boundary In
	@Test
	public void testConvertFromFahrenheitBoundaryIn() {
		int c  = Celcius.convertFromFahrenheit(7);
		assertTrue("Invalid fahrenheit temperature", c == -13);
	}
	
	// Boundary Out
	@Test
	public void testConvertFromFahrenheitBoundaryOut() {
		int c  = Celcius.convertFromFahrenheit(6);
		assertTrue("Valid fahrenheit temperature", c == -14);
	}
	

}
